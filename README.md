# README #

A standalone zlto app that demostrate the zlto API integration into 3rd party Client websites.


### How do I get set up? ###

> cd ./zlto-standalone

> virtualenv -p python3 ./env

> source ./env/bin/activate

> pip install -r ./requirements.txt

> cd ./zltostandalone

> ./manage.py makemigrations && ./manage.py migrate

> ./manage.py createsuperuser


### Project Owners

- Kurt Appolis (Tech Lead) - kurt@rlabs.org


## Setting up your own instance.


> Update the ZLTO_CONFIG dictionary by only change the 'partner_id' variable with the ID issued to you by the Zlto Team.

> Each API call is located in the zlto/utils/api_calls.py

> An example of each call in located in a zlto/tests.ZltoAPITestCase class.

> Testing on local, you may want to enable CORS. There are helpful browser extension that facilates this for you. 

> You can run http://localhost:8000 to preview a basic sandbox page with suggestion javascript implementation.


## Deployment

> When you ready to deploy, you will have to communicate with the Zlto team to whitelist your Domain to interact with our API. 


## Testing API Calls

### 1. Run the whole set of test functions for the zlto app

> ./manage.py test zlto


### 2. Run specific test function for the zlto app.


> ./manage.py test zlto.tests.ZltoAPITestCase.test_auth_login
