"""Zlto api integration library.

Created by: Kurt Appolis
email: kurt@rlabs.org
version: 1 (updated on 25 July 2018, 11:11)
"""

from django import forms


class ZltoNewAccountForm(forms.Form):
    """A new zlto account form class."""

    name = forms.CharField(required=True, label="Name")
    cell_number = forms.CharField(required=True, label="Contact Number")
    email = forms.EmailField(required=True, label="Email Address")
    password = forms.CharField(required=True, widget=forms.PasswordInput)
    confirm_password = forms.CharField(required=True, widget=forms.PasswordInput)

    surname = forms.CharField(required=False, label="Surname")
    partner_id = forms.IntegerField(required=False, label="Partner reference ID.")


class ZltoNewEarnActivity(forms.Form):
    """A new earn service / activity form class."""

    category = forms.IntegerField(required=True, label="A corresponding category id")
    partner = forms.IntegerField(required=True, label="A linked partner_id")

    title = forms.CharField(required=True)
    description = forms.CharField(required=True)

    reference_name = forms.CharField(required=True)
    reference_contact_number = forms.CharField(required=True)

    number_of_people = forms.IntegerField(required=True)
    number_of_hours = forms.IntegerField(required=True)
