"""Zlto views."""

from django.views.generic import TemplateView


class HomePageTemplateView(TemplateView):
    """Home Page template view."""

    template_name = "zlto/home.html"

    def get_context_data(self, **kwargs):
        """Default Context Data function."""
        context = super(HomePageTemplateView, self).get_context_data(**kwargs)
        from django.conf import settings
        context['zlto_api'] = settings.ZLTO_CONFIG['url']
        context['partner_id'] = settings.ZLTO_CONFIG['partner_id']
        return context
