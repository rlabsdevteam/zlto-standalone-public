"""Zlto api integration library.

Created by: Kurt Appolis
email: kurt@rlabs.org
version: 1.2 (updated on 12 Sept 2018, 15.41)
"""

from django.conf import settings

from zlto.forms import (ZltoNewAccountForm,
                        ZltoNewEarnActivity)

import requests

ZLTO_API_URL = settings.ZLTO_CONFIG['url']
ZLTO_PARTNER_ID = settings.ZLTO_CONFIG['partner_id']


def auth_login(*args, **kwargs):
    """Login into your zlto wallet.

    Required Fields:
    1. username
    2. password
    """
    username = kwargs['username'] or None
    password = kwargs['password'] or None

    if username is None:
        return {"status": 500, "results": "No username"}
    if password is None:
        return {"status": 500, "results": "No Password"}

    url = ZLTO_API_URL.format('api/login/')
    try:
        response = requests.post(url, json={'username': str(username), 'password': str(password)})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def auth_token_refresh(*args, **kwargs):
    """Refresh zlto access token.

    Required Headers
    1. Authorization: Bearer [Access Token]

    Required Fields
    2. refresh - refresh token
    """
    access_token = kwargs['access_token'] or None
    refresh = kwargs['refresh'] or None

    if access_token is None:
        return {"status": 500, "results": "No access token"}
    if refresh is None:
        return {"status": 500, "results": "No refresh token"}

    url = ZLTO_API_URL.format('api/login/refresh/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.post(url, headers=header, json={"refresh": refresh})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def create_account(*args, **kwargs):
    """Create a new zlto account.

    Required Fields:
    1. name
    2. email - this field will serve as client's username.
    3. password
    4. cell_number
    5. confirm_password

    Optional Fields
    1. surname
    2. partner_id - for partners that wants to track user signups.
    """
    name = kwargs['name'] or None
    email = kwargs['email'] or None
    password = kwargs['password'] or None
    confirm_password = kwargs['confirm_password'] or None
    cell_number = kwargs['cell_number'] or None
    surname = kwargs['surname'] or None
    partner_id = ZLTO_PARTNER_ID or None

    form = ZltoNewAccountForm(data={"name": name,
                                    "email": email,
                                    "password": password,
                                    "confirm_password": confirm_password,
                                    "cell_number": cell_number,
                                    "surname": surname,
                                    "partner_id": partner_id})

    if form.is_valid():
        url = ZLTO_API_URL.format('api/create/')
        try:
            response = requests.post(url, data=form.data)
            return {'status': response.status_code, 'results': response.json()}
        except Exception as e:
            return {'status': 500, 'results': e}
    else:
        return form.errors.as_json()


def perform_earn_activity(*args, **kwargs):
    """Send a post to insert a new earn activity on user's profile."""
    from datetime import datetime
    access_token = kwargs['access_token'] or None

    category_id = kwargs['category_id'] or None
    partner_id = ZLTO_PARTNER_ID or None

    title = kwargs['title'] or None
    description = kwargs['description'] or None

    reference_name = kwargs['reference_name'] or None
    reference_contact_number = kwargs['reference_contact_number'] or None

    number_of_people = kwargs['number_of_people'] or None
    number_of_hours = kwargs['number_of_hours'] or None
    amount = kwargs['amount'] or 0

    image_before = kwargs['image_before'] or None
    if image_before is None:
        return {'status': 500, 'results': "No Before Image"}

    image_after = kwargs['image_after'] or None
    if image_after is None:
        return {'status': 500, 'results': "No After Image"}

    form = ZltoNewEarnActivity(data={'category': category_id,
                                     'partner': partner_id,
                                     'title': title,
                                     'description': description,
                                     'reference_name': reference_name,
                                     'reference_contact_number': reference_contact_number,
                                     'number_of_people': number_of_people,
                                     'amount': amount,
                                     'date_performed': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                     'number_of_hours': number_of_hours})
    if form.is_valid():
        files = {'image_before': ('image_before.png', image_before),
                 'image_after': ('image_after.png', image_after)}

        url = ZLTO_API_URL.format('api/earn/create/')
        try:
            header = {'Authorization': 'Bearer {}'.format(access_token)}
            response = requests.post(url, headers=header, data=form.data, files=files)

            return {'status': response.status_code, 'results': response.json()}
        except Exception as e:
            return {'status': 500, 'results': e}
    else:
        return form.errors.as_json()


def perform_spend_activity(*args, **kwargs):
    """Purchase an item from a partner's store listing."""
    access_token = kwargs['access_token'] or None
    zlto_item_purchase_id = kwargs['zlto_item_purchase_id'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}
    if zlto_item_purchase_id is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/store/purchase/{}'.format(zlto_item_purchase_id))
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_profile_details(*args, **kwargs):
    """Retrieve auth users profile details."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/client/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_profile_details_by_uuid(*args, **kwargs):
    """Retrieve a profile details."""
    access_token = kwargs['access_token'] or None
    uuid = kwargs['uuid'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/client/%s/' % (uuid))
    print("url", url)
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_earn_activity_category_list(*args, **kwargs):
    """Retrieve a list of earn categories."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/earn/category/list/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_purchased_coupon_history(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/history/coupons/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_purchased_log_history(*args, **kwargs):
    """Retrieve a user's transaction history log."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/history/transactions/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_listed_opportunities(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/opportunity/list/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_listed_opportunity_by_partner(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None
    partner_id = ZLTO_PARTNER_ID

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/opportunity/list/partner/%s/' % (partner_id))
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_listed_opportunity_detail(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None
    opportunity_id = kwargs['opportunity_id'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/opportunity/detail/%s/' % (opportunity_id))
    print("url", url)
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_listed_opportunity_users(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None
    opportunity_id = kwargs['opportunity_id'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/opportunity/clients/list/%s/' % (opportunity_id))
    print("url", url)
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_partners_users(*args, **kwargs):
    """Retrieve a user's coupon log."""
    access_token = kwargs['access_token'] or None
    partner_id = ZLTO_PARTNER_ID

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/clients/%s/' % (partner_id))
    print("url", url)
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}


def get_profile_stats(*args, **kwargs):
    """Retrieve profile users base stats."""
    access_token = kwargs['access_token'] or None

    if access_token is None:
        return {'status': 500, 'results': "No access token"}

    url = ZLTO_API_URL.format('api/base/stats/')
    try:
        header = {'Authorization': 'Bearer {}'.format(access_token)}
        response = requests.get(url, headers=header, json={})
        return {'status': response.status_code, 'results': response.json()}
    except Exception as e:
        return {'status': 500, 'results': e}
