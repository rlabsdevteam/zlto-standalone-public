"""Zlto admin views."""

from django.contrib import admin

from .models import (ZltoAuthUserToken,
                     ZltoStoreItem)


@admin.register(ZltoAuthUserToken)
class ZltoAuthUserTokenAdmin(admin.ModelAdmin):
    """ZltoAuthUserToken admin view."""

    list_display = ('pk',
                    'username',
                    'timestamp',
                    'timestamp_updated')
    search_fields = ('username',)


@admin.register(ZltoStoreItem)
class ZltoStoreItemAdmin(admin.ModelAdmin):
    """ZltoStoreItem admin view."""

    list_display = ('pk',
                    'linkage_id',
                    'zlto_item_purchase_id',
                    'zlto_item_title',
                    'timestamp',
                    'timestamp_updated')
    list_filter = ('timestamp',
                   'timestamp_updated')
    search_fields = ('zlto_item_title',)
