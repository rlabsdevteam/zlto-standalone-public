"""Zlto API tables."""

from django.db import models


class ZltoAuthUserToken(models.Model):
    """Zlto Auth User login details."""

    username = models.CharField(max_length=250, unique=True, help_text="A user's zlto username")
    access_token = models.TextField(default=None, null=True, blank=True, help_text="The saved your's access tokeen\
        used when making further API calls")
    refresh_token = models.TextField(default=None, null=True, blank=True, help_text="A refresh token necessary to refresh\
        the access token to retain session.")

    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """To string function."""
        return "{}".format(self.username)


class ZltoStoreItem(models.Model):
    """Store items associated with zlto item groups."""

    zlto_item_purchase_id = models.IntegerField(blank=False, null=False, help_text="An Zlto code of an item to purchase.")
    zlto_item_title = models.CharField(max_length=250, help_text="The zlto code's title")
    linkage_id = models.IntegerField(blank=False, null=False, help_text="An ID field to link a system's item to the Zlto code.")

    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """To string function."""
        return "{}".format(self.zlto_item_title)

    class Meta:
        """ZltoStoreItem meta class."""

        unique_together = [("zlto_item_purchase_id", "linkage_id")]
