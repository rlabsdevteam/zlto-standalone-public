"""zlto URL Configuration."""
from django.urls import path
from .views import (HomePageTemplateView)

app_name = "zlto"

urlpatterns = [
    path('', HomePageTemplateView.as_view(), name="index"),
]
