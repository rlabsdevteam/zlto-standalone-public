"""A test case to check all the calls."""

from django.test import TestCase
from unittest import skip
from .utils.api_calls import (auth_login,
                              auth_token_refresh,
                              create_account,
                              get_profile_details,
                              get_earn_activity_category_list,
                              perform_earn_activity,
                              perform_spend_activity,
                              get_purchased_coupon_history,
                              get_purchased_log_history,
                              get_listed_opportunities,
                              get_listed_opportunity_by_partner,
                              get_listed_opportunity_detail,
                              get_partners_users,
                              get_listed_opportunity_users,
                              get_profile_stats,
                              get_profile_details_by_uuid)

from .models import (ZltoAuthUserToken,
                     ZltoStoreItem)


class ZltoAPITestCase(TestCase):
    """Zlto API Integration test case."""

    account = {"username": None,
               "password": None}

    def setUp(self):
        """Set up the test case with user details."""
        self.account['username'] = "-username/email-"
        self.account['password'] = "-password-"

    def test_auth_login(self):
        """Test Auth login."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_auto_login_insert(self):
        """Test logging in and inserting."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        ZltoAuthUserToken.objects.create(username=self.account['username'],
                                         access_token=response['results']['access'],
                                         refresh_token=response['results']['refresh'])

        record = ZltoAuthUserToken.objects.get(username=self.account['username'])

        self.assertEqual(record.username, self.account['username'])

    def test_auto_refresh(self):
        """Test Autho Access token refreshing."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        ZltoAuthUserToken.objects.create(username=self.account['username'],
                                         access_token=response['results']['access'],
                                         refresh_token=response['results']['refresh'])

        record = ZltoAuthUserToken.objects.get(username=self.account['username'])

        response = auth_token_refresh(access_token=record.access_token,
                                      refresh=record.refresh_token)
        print(response)
        self.assertEqual(response['status'], 200)

    @skip("Dont need to create an account anymore.")
    def test_create_account(self):
        """Test creating an account."""
        email = 'appolis.kurt30@gmail.com'
        response = create_account(name='kurt2',
                                  surname='appolis',
                                  email=email,
                                  cell_number="093498234",
                                  password='newpassword',
                                  confirm_password='newpassword')

        self.assertEqual(response['status'], 201)

        ZltoAuthUserToken.objects.create(username=email,
                                         access_token=response['results']['token'],
                                         refresh_token=None)

        record = ZltoAuthUserToken.objects.get(username=email)

        self.assertEqual(record.username, email)

    def test_get_profile_details(self):
        """Retrieve profile details."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])

        ZltoAuthUserToken.objects.create(username=self.account['username'],
                                         access_token=response['results']['access'],
                                         refresh_token=response['results']['refresh'])

        record = ZltoAuthUserToken.objects.get(username=self.account['username'])

        self.assertEqual(record.username, self.account['username'])

        response = get_profile_details(access_token=record.access_token)
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_earn_activity_category_list(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        response = get_earn_activity_category_list(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    @skip("Dont need to keep sending an earn activity.")
    def test_perform_earn_activity(self):
        """Post a new earn activity."""
        from django.conf import settings

        image_path = "{}/media/zlto_coin.png".format(settings.BASE_DIR)

        image_before = open(image_path, 'rb').read()
        image_after = open(image_path, 'rb').read()

        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        response = perform_earn_activity(access_token=response['results']['access'],
                                         category_id=1,
                                         title="New earn activity",
                                         description="Here is the description",
                                         reference_name="Kurt Appolis",
                                         reference_contact_number="0813123123",
                                         number_of_people=12,
                                         number_of_hours=1,
                                         amount=0,
                                         image_before=image_before,
                                         image_after=image_after,)
        print(response)
        self.assertEqual(response['status'], 201)

    # @skip("Dont need to keep spending.")
    def test_perform_spend_activity(self):
        """Purchase an item from zlto activity."""
        is_record = ZltoStoreItem.objects.create(zlto_item_purchase_id=110,
                                                 zlto_item_title="Filter Coffee",
                                                 linkage_id=113)

        self.assertEqual(is_record.zlto_item_title, "Filter Coffee")

        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        self.assertEqual(response['status'], 200)

        response = perform_spend_activity(access_token=response['results']['access'],
                                          zlto_item_purchase_id=is_record.zlto_item_purchase_id)
        print(response)
        self.assertEqual(response['status'], 202)

    def test_get_purchased_coupon_history(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        print(response)
        response = get_purchased_coupon_history(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_purchased_log_history(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        print(response)
        response = get_purchased_log_history(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_listed_opportunities(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        print(response)
        response = get_listed_opportunities(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_listed_opportunity_by_partner(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        print(response)
        response = get_listed_opportunity_by_partner(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_listed_opportunity_details(self):
        """Retrieve a list of earn categories."""
        auth_response = auth_login(username=self.account['username'],
                                   password=self.account['password'])
        print(auth_response)
        response = get_listed_opportunity_by_partner(access_token=auth_response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

        if response['status'] == 200:
            if len(response['results']['results']) > 0:
                one_opportunity = response['results']['results'][0]
                response = get_listed_opportunity_detail(access_token=auth_response['results']['access'],
                                                         opportunity_id=one_opportunity['id'])
                print(response)
                self.assertEqual(response['status'], 200)

    def test_get_listed_opportunity_users(self):
        """Retrieve a list of earn categories."""
        auth_response = auth_login(username=self.account['username'],
                                   password=self.account['password'])
        response = get_listed_opportunity_by_partner(access_token=auth_response['results']['access'])

        self.assertEqual(response['status'], 200)

        if response['status'] == 200:
            if len(response['results']['results']) > 0:
                one_opportunity = response['results']['results'][0]
                response = get_listed_opportunity_users(access_token=auth_response['results']['access'],
                                                        opportunity_id=one_opportunity['id'])
                print(response)
                self.assertEqual(response['status'], 200)

    def test_get_partners_users(self):
        """Retrieve a list of earn categories."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])
        response = get_partners_users(access_token=response['results']['access'])
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_profile_stats(self):
        """Retrieve profile base details details."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])

        ZltoAuthUserToken.objects.create(username=self.account['username'],
                                         access_token=response['results']['access'],
                                         refresh_token=response['results']['refresh'])

        record = ZltoAuthUserToken.objects.get(username=self.account['username'])

        response = get_profile_stats(access_token=record.access_token)
        print(response)
        self.assertEqual(response['status'], 200)

    def test_get_profile_details_by_uuid(self):
        """Retrieve profile details by uuid."""
        response = auth_login(username=self.account['username'],
                              password=self.account['password'])

        ZltoAuthUserToken.objects.create(username=self.account['username'],
                                         access_token=response['results']['access'],
                                         refresh_token=response['results']['refresh'])

        record = ZltoAuthUserToken.objects.get(username=self.account['username'])

        response = get_profile_details(access_token=record.access_token)

        self.assertEqual(response['status'], 200)

        response2 = get_profile_details_by_uuid(access_token=record.access_token,
                                                uuid=response['results']['profile']['uuid_id'])

        print("response2", response2)
        self.assertEqual(response2['status'], 200)
